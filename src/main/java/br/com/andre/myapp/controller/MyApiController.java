package br.com.andre.myapp.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/")
public class MyApiController {
	
	@GetMapping("health")
	public Object health() {
		System.out.println("call api");
		Map<String, String> map = new HashMap<>();
		map.put("status", "OK");
		return map;
	}
	
}
